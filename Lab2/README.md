#Lab2
###Run:
``` 
	cd <Lab2 location>
	ghci Main
	:main data1.txt
	:main data2.txt
```
###Run tests:
1. Download HUnit
2. In console:
```
	cd <Lab2 location>
	ghci -package HUnit Tests
	main

