{-# LANGUAGE OverloadedStrings #-}
module User where

import Database.MySQL.Base
import qualified System.IO.Streams as Streams

createUser name conn = do
    query <- prepareStmt conn "insert into User (`name`) values (?)"
    _ <- executeStmt conn query [MySQLText name]
    query <- prepareStmt conn "SELECT userId FROM user ORDER BY userId DESC LIMIT 1"
    (defs, is) <- queryStmt conn query []
    list <- (Streams.toList is)
    case ((list !! 0)) of [MySQLInt32 x] -> return x;
                          _ -> error "bad value";

readUser id conn = do
    query <- prepareStmt conn "select `name` from user where userId = ?"
    (defs, is) <- queryStmt conn query [MySQLInt32 id]
    list <- (Streams.toList is)
    case ((list !! 0)) of [MySQLText x] -> print x;
                          _ -> error "bad value";

updateUser id name conn = do
    query <- prepareStmt conn "update User set name = ? where userId = ?"
    _ <- executeStmt  conn query [MySQLText name, MySQLInt32 id]
    return()

deleteUser id conn = do 
    query <- prepareStmt conn "delete from User where `userId` = ?"
    _ <- executeStmt conn query [MySQLInt32 id]
    return()
