{-# LANGUAGE OverloadedStrings #-}
import Test.HUnit
import Database.MySQL.Base
import qualified System.IO.Streams as Streams
import Control.Monad
import Program_User
import User
import Author
import Program


makeConn = connect defaultConnectInfo {ciUser = "root", ciPassword = "admin", ciDatabase = "faculty_network"}

authorTest = TestCase (do conn <- makeConn
                          aid <- createAuthor "authorMain" 5 conn
                          _ <- readAuthor aid conn
                          _ <- readAuthorAll conn
                          _ <- updateAuthor aid "NEWNAMEAUTHOR" 7 conn
                          _ <- deleteAuthor aid conn
                          return ())

userTest = TestCase (do conn <- makeConn
                        uid <- createUser "userMain" conn
                        _ <- readUser uid conn
                        _ <- updateUser uid "NEWNAMEUSER" conn
                        return ())


programTest = TestCase (do conn <- makeConn
                           pid <- createProgram "ProgramMain" 5 "anotation" "1.1.1" conn
                           _ <- readProgram pid conn
                           _ <- readProgramAll conn
                           _ <- updateProgram pid "NEWNAMEPROGRAM" 6 "ano" "123" conn
                           return ())

programUserTest = TestCase (do conn <- makeConn
                               _ <- addProgramUser 1 1 conn
                               _ <- printAllUsers 1 conn
                               _ <- printAllPrograms 1 conn
                               return ())



main :: IO Counts
main = runTestTT $ TestList [authorTest, userTest, programTest, programUserTest]