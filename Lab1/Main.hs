{-# LANGUAGE OverloadedStrings #-}

module Main where

import Program_User
import User
import Author
import Program


import Database.MySQL.Base
import qualified System.IO.Streams as Streams


main :: IO () 
main = do
    conn <- connect
        defaultConnectInfo {ciUser = "root", ciPassword = "admin", ciDatabase = "faculty_network"}

    uid <- createUser "userMain" conn
    _ <- readUser uid conn
    _ <- updateUser uid "NEWNAMEUSER" conn
   

    aid <- createAuthor "authorMain" 5 conn
    _ <- readAuthor aid conn
    _ <- readAuthorAll conn
    _ <- updateAuthor aid "NEWNAMEAUTHOR" 7 conn
    _ <- deleteAuthor aid conn

    pid <- createProgram "ProgramMain" 5 "anotation" "1.1.1" conn
    _ <- readProgram pid conn
    _ <- readProgramAll conn
    _ <- updateProgram pid "NEWNAMEPROGRAM" 6 "ano" "123" conn

    _ <- addProgramUser uid pid conn
    _ <- printAllUsers 1 conn
    _ <- printAllPrograms 1 conn


    print "End"

