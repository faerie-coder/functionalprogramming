{-# LANGUAGE OverloadedStrings #-}
module Program where
    
import Database.MySQL.Base
import qualified System.IO.Streams as Streams
import Control.Monad

printProgram name authorId anotation version = do
    print name
    print authorId
    print anotation
    print version
    print ""

createProgram name authorId annotaion version conn = do
    query <- prepareStmt conn "insert into Program (`name`, `authorId`, `anotation`, `version`) values (?, ?, ? ,?)"
    _ <- executeStmt conn query [MySQLText name, MySQLInt32 authorId, MySQLText annotaion, MySQLText version]
    query <- prepareStmt conn "SELECT programId FROM program ORDER BY programId DESC LIMIT 1"
    (defs, is) <- queryStmt conn query []
    list <- (Streams.toList is)
    case ((list !! 0)) of [MySQLInt32 x] -> return x;
                          _ -> error "bad value";

readProgram id conn = do
    query <- prepareStmt conn "select `name`, `authorId`, `anotation`, `version` from Program where ProgramId = ?"
    (defs, is) <- queryStmt conn query [MySQLInt32 id]
    list <- (Streams.toList is)
    print list
    case ((list !! 0)) of [MySQLText x, MySQLInt32 y, MySQLText z, MySQLText t] -> printProgram x y z t;
                          _ -> error "bad value";

readProgramAll conn = do
    query <- prepareStmt conn "select `name`, `authorId`, `anotation`, `version` from Program"
    (defs, is) <- queryStmt conn query []
    list <- (Streams.toList is)
    forM_ list (\elem -> do
        case (elem) of [MySQLText x, MySQLInt32 y, MySQLText z, MySQLText t] -> printProgram x y z t;
                       _ -> error "bad value";)

updateProgram id  name authorId anotation version conn = do
    query <- prepareStmt conn "update Program set `name` = ?, `authorId` = ?, `anotation` = ?, `version` = ? where programId = ?"
    _ <- executeStmt conn query [MySQLText name, MySQLInt32 authorId, MySQLText anotation, MySQLText version, MySQLInt32 id]
    return()

deleteProgram id conn = do 
    query <- prepareStmt conn "delete from Program where programId = ?"
    _ <- executeStmt conn query [MySQLInt32 id]
    return()
