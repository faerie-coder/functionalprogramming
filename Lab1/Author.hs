{-# LANGUAGE OverloadedStrings #-}
module Author where
    
import Database.MySQL.Base
import qualified System.IO.Streams as Streams
import Control.Monad

createAuthor name rank conn = do
    query <- prepareStmt conn "insert into Author (`name`, `rank`) values (?, ?)"
    _ <- executeStmt conn query [MySQLText name, MySQLInt32 rank]
    query <- prepareStmt conn "SELECT authorId FROM author ORDER BY authorId DESC LIMIT 1"
    (defs, is) <- queryStmt conn query []
    list <- (Streams.toList is)
    case ((list !! 0)) of [MySQLInt32 x] -> return x;
                          _ -> error "bad value";

readAuthor id conn = do
    query <- prepareStmt conn "select name from Author where authorId = ?"
    (defs, is) <- queryStmt conn query [MySQLInt32 id]
    list <- (Streams.toList is)
    case ((list !! 0)) of [MySQLText x] -> print x;
                          _ -> print "No Author";

readAuthorAll conn = do
    query <- prepareStmt conn "select name from Author"
    (defs, is) <- queryStmt conn query []
    list <- (Streams.toList is)
    forM_ list (\elem -> do
        case ((elem)) of [MySQLText x] -> print x;
                            _ ->  print "No Author";)

updateAuthor id name rank conn = do
    query <- prepareStmt conn "update Author set `name` = ?, `rank` = ? where authorId = ?"
    _ <- executeStmt conn query [MySQLText name, MySQLInt32 rank, MySQLInt32 id]
    return()

deleteAuthor id conn = do 
    query <- prepareStmt conn "delete from Author where authorId = ?"
    _ <- executeStmt  conn query [MySQLInt32 id]
    return()
