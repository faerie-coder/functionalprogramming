{-# LANGUAGE OverloadedStrings #-}
module Program_User where

import Database.MySQL.Base
import qualified System.IO.Streams as Streams
import Control.Monad

printProgram name authorId anotation version = do
    print name
    print authorId
    print anotation
    print version
    print ""

addProgramUser programId userId conn = do
    query <- prepareStmt conn "insert into programuser (`programId`, `userId`) values (?, ?)"
    _ <- executeStmt conn query [MySQLInt32 programId, MySQLInt32 userId]
    return()

printAllUsers programId conn = do
    query <- prepareStmt conn "select name from user join programuser where (user.userId = programuser.userId and programuser.programId = ?)"
    (defs, is) <- queryStmt conn query [MySQLInt32 programId]
    list <- (Streams.toList is)
    forM_ list (\elem -> do
        case ((elem)) of [MySQLText x] -> print x;
                         _ -> error "bad value";)

printAllPrograms userId conn = do
    query <- prepareStmt conn "select `name`, `authorId`, `anotation`, `version` from program join programuser where (program.programId = programuser.programId and programuser.userId = ?)"
    (defs, is) <- queryStmt conn query [MySQLInt32 userId]
    list <- (Streams.toList is)
    forM_ list (\elem -> do
        case (elem) of [MySQLText x, MySQLInt32 y, MySQLText z, MySQLText t] -> printProgram x y z t;
                       _ -> error "bad value";)

