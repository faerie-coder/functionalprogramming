USE faculty_network;

CREATE TABLE IF NOT EXISTS `Author`(
	`authorId` int NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	`rank` varchar(10) NOT NULL,
	PRIMARY KEY (`authorId`)
);
	
insert into `Author` (`name`, `rank`) values
('author1', '4'),
('author2', '5'),
('author3', '8');

CREATE TABLE IF NOT EXISTS `User`(
	`userId` int NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	PRIMARY KEY (`userId`)
);

insert into `User` (`name`) values
('user1'),
('user2'),
('user3');
	
CREATE TABLE IF NOT EXISTS `Program`(
	`programId` int NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	`authorId` int,
	`anotation` varchar(1000),
	`version` varchar(10),
	PRIMARY KEY (`programId`),
	FOREIGN KEY (`authorId`) REFERENCES `Author`(`authorId`) ON DELETE CASCADE
);


insert into `Program` (`name`, `authorId`, `anotation`, `version`) values
('program1', '3', 'anotation', '1.1'),
('program2', '2', 'anotation', '1.6'),
('program3', '2', 'anotation', '5.6'),
('program4', '1', 'anotation', '2.2');

CREATE TABLE IF NOT EXISTS `ProgramUser`(
	`programId` int NOT NULL,
	`userId` int NOT NULL,
	FOREIGN KEY (`programId`) REFERENCES `User`(`userId`) ON DELETE RESTRICT ON UPDATE CASCADE,
	FOREIGN KEY (`userId`) REFERENCES `Program`(`programId`) ON DELETE RESTRICT ON UPDATE CASCADE,
	PRIMARY KEY (`programId`, `userId`)
);

insert into `ProgramUser` (`programId`, `userId`) values
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 3);


	